import React from "react";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Paymentmask from "./Components/Paymentmask";
import Paymentinfo from "./Components/Paymentinfo";
import Paymentrecieved from "./Components/Paymentrecieved";
import { Route, BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="h-screen">
        <Header />
        <Route path="/" exact component={Paymentmask} />
        <Route path="/payment" exact component={Paymentinfo} />
        <Route path="/test-payment" exact component={Paymentrecieved} />

        <Footer />
      </div>
    </Router>
  );
}

export default App;
